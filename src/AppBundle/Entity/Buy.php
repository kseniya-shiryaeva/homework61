<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Buy
 *
 * @ORM\Table(name="buy")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BuyRepository")
 */
class Buy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="buydate", type="datetime")
     *
     * @var string
     */
    private $buydate;

    /**
     * @var Dish
     *
     * @ORM\ManyToOne(targetEntity="Dish", inversedBy="buys")
     * @ORM\JoinColumn(name="dish", referencedColumnName="id")
     */
    private $dish;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Dish $dish
     * @return Buy
     */
    public function setDish($dish)
    {
        $this->dish = $dish;
        return $this;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @param mixed $buydate
     * @return Buy
     */
    public function setBuydate($buydate)
    {
        $this->buydate = $buydate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuydate()
    {
        return $this->buydate;
    }
}

