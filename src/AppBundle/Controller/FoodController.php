<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Institution;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FoodController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $institutions = $this->getDoctrine()->getRepository('AppBundle:Institution')->findAll();

        $inst_food = [];

        foreach ($institutions as $key => $institution){
            $inst_food[$key]['institution'] = $institution;
            $inst_food[$key]['dishes'] = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getPopularDishesByInstitution($institution->getId());
        }

        return $this->render('@App/Food/index.html.twig', array(
            'inst_foods' => $inst_food
        ));
    }

    public function dishesAction($inst)
    {
        $dishes = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Dish')
            ->getDishesByInstitution($inst);

        return $dishes;
    }

    public function PopularDishesAction($inst)
    {
        $dishes = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Dish')
            ->getPopularDishesByInstitution($inst);

        return $dishes;
    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function InstitutionAction($id)
    {
        $institution = $this->getDoctrine()->getRepository('AppBundle:Institution')->find($id);

        return $this->render('@App/Food/institution.html.twig', array(
            'institution' => $institution,
            'dishes' => $this->dishesAction($id)
        ));
    }

}
