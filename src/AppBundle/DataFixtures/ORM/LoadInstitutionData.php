<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 31.05.18
 * Time: 16:28
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Institution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadInstitutionData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $institutions = [
            ['title' => 'Тесто Место', 'foto' => 'testo-mesto.png', 'description' => 'Какое-то длинное описание Тесто Место.'],
            ['title' => 'Sova', 'foto' => 'Sova.png', 'description' => 'Какое-то длинное описание Sova.'],
            ['title' => 'Buffet', 'foto' => 'Buffet.jpeg', 'description' => 'Какое-то длинное описание Buffet.'],
        ];


        foreach ($institutions as $key=>$institution){

            $inst = new Institution();

            $inst
                ->setTitle($institution['title'])
                ->setFoto($institution['foto'])
                ->setDescription($institution['description']);

            $manager->persist($inst);

            $this->addReference('inst_'.$key, $inst);
        }

        $manager->flush();
    }
}