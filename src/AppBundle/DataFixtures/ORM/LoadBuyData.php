<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 31.05.18
 * Time: 16:29
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Buy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBuyData extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $buys = [];


        for ($i=0; $i<20; $i++){

            $key = 'dish_'.$i;

            $inst = $this->getReference($key);

            $nowDate = new \DateTime();
            $oldDate = new \DateTime();
            $oldDate->modify('-1 year');


            $randCount = rand(1, 10);

            for($j=0; $j<$randCount; $j++){
                $rand_date = $this->randomDateInRange($oldDate, $nowDate);
                $buys[] = ['date' => $rand_date, 'dish' => $inst];
            }

            $oldDate->modify('-5 year');
            $nowDate->modify('-1 year');
            $rand_date = $this->randomDateInRange($oldDate, $nowDate);
            $buys[] = ['date' => $rand_date, 'dish' => $inst];


        }

        foreach ($buys as $buy){
            $one_buy = new Buy();

            $one_buy
                ->setBuydate($buy['date'])
                ->setDish($buy['dish']);

            $manager->persist($one_buy);
        }

        $manager->flush();

    }

    public function getDependencies()

    {

        return array(

            LoadDishData::class,

        );

    }

    protected function randomDateInRange(\DateTime $start, \DateTime $end) {
        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new \DateTime();
        $randomDate->setTimestamp($randomTimestamp);

        return $randomDate;
    }
}