<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 31.05.18
 * Time: 16:28
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishData extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $inst0 = $this->getReference('inst_0');
        $inst1 = $this->getReference('inst_1');
        $inst2 = $this->getReference('inst_2');

        $dishes = [
            ['title' => 'Пельмени', 'price' => 165, 'foto' => '1pelmeni.png', 'institution' => $inst0],
            ['title' => 'Манты', 'price' => 160, 'foto' => '1manti.png', 'institution' => $inst0],
            ['title' => 'Вареники', 'price' => 130, 'foto' => '1vareniki.png', 'institution' => $inst0],
            ['title' => 'Вареники с вишней', 'price' => 130, 'foto' => '1varenikivishnia.png', 'institution' => $inst0],
            ['title' => 'Чизкейк', 'price' => 100, 'foto' => '2cezar.png', 'institution' => $inst0],
            ['title' => 'Рыбное ассорти', 'price' => 165, 'foto' => '2ribnoeassorti.png', 'institution' => $inst1],
            ['title' => 'Салат Сытный', 'price' => 150, 'foto' => '2salat-sitnii.png', 'institution' => $inst1],
            ['title' => 'Шпиг', 'price' => 200, 'foto' => '2shpig.png', 'institution' => $inst1],
            ['title' => 'Мимоза', 'price' => 120, 'foto' => '2mimoza.png', 'institution' => $inst1],
            ['title' => 'Рулет', 'price' => 150, 'foto' => '2rulet.png', 'institution' => $inst1],
            ['title' => 'Курдак', 'price' => 165, 'foto' => '2kurdak.png', 'institution' => $inst1],
            ['title' => 'Ганфан', 'price' => 170, 'foto' => '2ganfan.png', 'institution' => $inst1],
            ['title' => 'Бризоль', 'price' => 90, 'foto' => '2brizol.png', 'institution' => $inst1],
            ['title' => 'Блины', 'price' => 70, 'foto' => '3blini.png', 'institution' => $inst2],
            ['title' => 'Борщ', 'price' => 90, 'foto' => '3borssh.png', 'institution' => $inst2],
            ['title' => 'Лагман', 'price' => 110, 'foto' => '3lagman.png', 'institution' => $inst2],
            ['title' => 'Гуляш', 'price' => 160, 'foto' => '3guliash.png', 'institution' => $inst2],
            ['title' => 'Котлета', 'price' => 120, 'foto' => '3kotleta.png', 'institution' => $inst2],
            ['title' => 'Плов', 'price' => 120, 'foto' => '3plov.png', 'institution' => $inst2],
            ['title' => 'Оливье', 'price' => 90, 'foto' => '3olivie.png', 'institution' => $inst2],
        ];

        foreach ($dishes as $key => $dish){

            $one_dish = new Dish();

            $one_dish
                ->setTitle($dish['title'])
                ->setPrice($dish['price'])
                ->setFoto($dish['foto'])
                ->setInstitution($dish['institution']);

            $manager->persist($one_dish);

            $this->addReference('dish_'.$key, $one_dish);
        }

        $manager->flush();
    }

    public function getDependencies()

   {

       return array(

          LoadInstitutionData::class,

      );

   }
}